import Darwin
import Foundation
import KeychainSwift
import CryptoSwift
import UIKit

let keychain = KeychainSwift()

/* // Store the previously created key
keychain.set(dataObject, forKey: "my key", withAccess: .accessibleWhenUnlockedThisDeviceOnly)

// Receive from keychain our key
let key = keychain.getData("my key")
 */

// run only when creating chat room
func d_h_initiate() -> {
    let p = gen_p()
    let g = gen_g()
    // store p and g in keychain
    let status = SecItemAdd(p, nil)
    guard status == errSecSuccess else {  }
    let status = SecItemAdd(g, nil)
    guard status == errSecSuccess else {  }

    let public = gen_Send_val(p, g)
    // store public in firebase so that other user can see
}

func calculate_hash_key_dh(_ secret: UInt64, _ p: UInt64) -> Data {
    // get a, p from keychain
    x = find_x(secret, p)
    let hash = x.sha512() // "123".bytes.md5()
    // store hash as d_h key
    return hash
}

func find_x(_ secret: UInt64, _ p: UInt64) -> UInt64 {
    // get send val of other recipient from firebase, UINT64
    var other_send_val = 1
    let x = UInt64(pow(Double(other_send_val), a) % p)
    return x
}

// Do below gen functions when first creating a chat room and store p, g, send_val
// in keychain.
// Do two times for two keys
func gen_Send_val(_ p: UInt64, _ g: UInt64) -> UInt64 {
    secret = UInt64.random(in 1...Int64.max)
    // keychain
    let status = SecItemAdd(secret, nil)
    guard status == errSecSuccess else {  }

    return UInt64(pow(double(g), secret) % p)
}

func gen_p() -> UInt64 {
    return nextSafePrime(UInt64.random(in: 2...UInt64.max))
}

func gen_g(_ p: UInt64) -> UInt64 {
    g_0 = UInt64.random(in: 2..(p-1))
    var g: UInt64
    repeat {
        g = pow(Double(g_0), 2) % p
    } while (g == 1)
    return g
}

func isSafePrime(_ p: UInt64) -> Bool {
    if (isPrime((p-1)/2)) {
        return true
    }
    return false
}

func isPrime(_ n: UInt64) -> Bool {
    let sqr = sqrt(n)
    for i in 2...sqr {
        if (i % n == 0) {
            return false;
        }
    }
    return true;
}

func nextSafePrime(_ n: UInt64) -> UInt64 {
    var p = n
    while (!isPrime(p) && !isSafePrime((p-1)/2)) {
        p += 1
    }
    return p
}


/* AES */
do {
  let aes = try AES(key: "passwordpassword", iv: "drowssapdrowssap") // aes128
  let ciphertext = try aes.encrypt(Array("Nullam quis risus eget urna mollis ornare vel eu leo.".utf8))
} catch { }

// HMAC

// get key from keychain, m is encrypted version of msgToSend
try HMAC(key: secret, variant: .sha256).authenticate(m)
