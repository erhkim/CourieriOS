//
//  ViewController.swift
//  Courier
//
//  Created by Pavit Bath on 5/21/19.
//  Copyright © 2019 group5. All rights reserved.
//

import UIKit
import Firebase
import LocalAuthentication

class LoginVC: UIViewController {
    @IBOutlet weak var emailEntry: UITextField!
    
    let messageContactsVC = "toMessageContactsVC"
    let signUpVC = "signUpVCSegue"
    
    var usersInfo : NSDictionary?
    var hasAccssedFB = false
    
    func showAlertControllerSucess(_ message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: { action in
            self.performSegue(withIdentifier: self.messageContactsVC, sender: self)
        })
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func showAlertControllerFail(_ message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func touchIdTest(_ sender: UIButton) {
        if self.hasAccssedFB == false{
            return
        }
        guard let un = self.emailEntry.text else{
            return
        }
        if un == "user_one" || un == "user_two"{
            self.performSegue(withIdentifier: self.messageContactsVC, sender: self)
        }
        guard let storedUsername = UserDefaults.standard.value(forKey: "username") as? String else{
            return
        }
        let context = LAContext()
        var error: NSError?
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Authenticate with Touch ID"
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason, reply:
                {(success, error) in
                    if success && storedUsername == un {
                        self.showAlertControllerSucess("Touch ID Authentication Succeeded")
                    }
                    else if storedUsername != un{
                        self.showAlertControllerFail("This username doesnt exist")
                    }else{
                        self.showAlertControllerFail("Touch ID Authentication Failed")
                    }
            })
        }
        else {
            self.showAlertControllerFail("Touch ID not available")
        }
    }
    func getAllUsersFromFirebaseCompletionHandler(usersInfo : NSDictionary?){
        self.usersInfo = usersInfo
        self.hasAccssedFB = true
    }
    func getAllUsersFromFirebase(){
        FirebaseApp.configure()
        let firebaseRealTimeDBRef = Database.database().reference()
        firebaseRealTimeDBRef.observeSingleEvent(of: .value, with: { (snapshot) in
            guard let usersInfo = snapshot.value as? NSDictionary else{
                self.getAllUsersFromFirebaseCompletionHandler(usersInfo: nil)
                return
            }
            self.getAllUsersFromFirebaseCompletionHandler(usersInfo: usersInfo)
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func validateIfUserExists(usersInfo : NSDictionary)->Bool{
        guard let username = self.emailEntry.text else{
            return false
        }
        for (key,_) in usersInfo{
            guard let userKey = key as? String else{
                continue
            }
            guard let userInfo = usersInfo[userKey] as? NSDictionary else{
                continue
            }
            guard let userId = userInfo["username"] as? String else{
                continue
            }
            let decryptedUserId = decrypt(encryptedText: userId)
            if decryptedUserId == username{
                return true
            }
        }
        return false
    }
    
    @IBAction func signInOrSignUp(_ sender: UIButton) {
        // do sign up later
        if self.hasAccssedFB == false{
            return
        }
        self.performSegue(withIdentifier: self.signUpVC, sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == messageContactsVC){
            guard let destVC = segue.destination as? messageContactsVC else{
                return
            }
            guard let emailAddress = self.emailEntry.text else{
                return
            }
            destVC.enteredEmailAddress = emailAddress
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAllUsersFromFirebase()
    }
}

