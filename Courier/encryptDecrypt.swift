//
//  encryptDecrypt.swift
//  Courier
//
//  Created by Sunil Ramakrishnan on 6/2/19.
//  Copyright © 2019 group5. All rights reserved.
//

import Foundation
import CryptoSwift
import Security

func create_public_key(id: String) -> String {
    let tag = ("key.private" + id).data(using: .utf8)!
    let attributes: [String: Any] =
        [kSecAttrKeyType as String:            kSecAttrKeyTypeRSA,
         kSecAttrKeySizeInBits as String:      2048,
         kSecPrivateKeyAttrs as String:
            [kSecAttrIsPermanent as String:    true,
             kSecAttrApplicationTag as String: tag]
    ]
    
    var error: Unmanaged<CFError>?
    let privateKey = SecKeyCreateRandomKey(attributes as CFDictionary, &error)
    
    
    let addquery: [String: Any] = [kSecClass as String: kSecClassKey,
                                    kSecAttrApplicationTag as String: tag,
                                    kSecValueRef as String: privateKey!]
    let status = SecItemAdd(addquery as CFDictionary, nil)
    print(status)
    
    
    let publicKey = SecKeyCopyPublicKey(privateKey!)
    
    let data = SecKeyCopyExternalRepresentation(publicKey!, &error)

    return (data! as Data).toHexString()
}


func encrypt(plainText : String)->String{
    // Stirng -> Encrypted Bytes -> Hex
    let key: Array<UInt8> = [0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00]
    let iv: Array<UInt8> = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
    var encryptedMsg: String = "_error_encrypting_".bytes.toHexString()
    do {
        let encrypted = try AES(key: key, blockMode: CBC(iv: iv), padding: .pkcs7).encrypt(plainText.bytes)
        encryptedMsg = encrypted.toHexString()
    } catch {
        print(error)
    }
    
    
    
    // HMAC
    let hmackey:Array<UInt8> = [1,2,3,4,5,6,7,8,9,10]
    var hmac: Array<UInt8> = []
    do {
        hmac = try HMAC(key: hmackey, variant: .sha256).authenticate(encryptedMsg.bytes)
        // message on db = encryptedmsg || hmac
        encryptedMsg += "{}" + hmac.toHexString()
    } catch {
        print(error)
    }
    return encryptedMsg
}

func decrypt(encryptedText : String)->String{
    // HMAC
    let hmackey:Array<UInt8> = [1,2,3,4,5,6,7,8,9,10]
    // message on db = encryptedmsg || hmac
    let decompose_msg = encryptedText.components(separatedBy: "{}")
    var encrypted_msg: String
    var remote_hmac: String
    if decompose_msg.count != 2 {
        encrypted_msg = encryptedText
        remote_hmac = ""
    } else {
        encrypted_msg = decompose_msg[0]
        remote_hmac = decompose_msg[1]
    }
    
    // Verify HMAC
    var hmac: Array<UInt8> = []
    do {
        hmac = try HMAC(key: hmackey, variant: .sha256).authenticate(encrypted_msg.bytes)
    } catch {
        print(error)
        return "hmac_failed"
    }
    
    if hmac.toHexString() != remote_hmac {
        return "hmac_failed"
    } else {
        print("Successful_HMAC")
        // AES Decrypt
        let key: Array<UInt8> = [0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00]
        
        let iv: Array<UInt8> = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
        
        do {
            let decrypted = try AES(key: key, blockMode: CBC(iv: iv), padding: .pkcs7).decrypt(Array<UInt8>(hex: encrypted_msg))
            guard let msg = String(bytes: decrypted, encoding: .utf8) else{
                return  "hmac_failed"
            }
            return msg
        } catch {
            print(error)
            return "hmac_failed"
        }
        
    }
}
