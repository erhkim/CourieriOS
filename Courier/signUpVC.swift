//
//  signUpVC.swift
//  Courier
//
//  Created by Sunil Ramakrishnan on 5/21/19.
//  Copyright © 2019 group5. All rights reserved.
//

import UIKit
import Firebase

class signUpVC: UIViewController {
    @IBOutlet weak var username: UITextField!
    var usersInfo : [String : Any]?
    var hasAccssedFB = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAllUsersFromFirebase()
    }
    func showAlertControllerDupe() {
        let alertController = UIAlertController(title: nil, message: "This username already exists", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    func showAlertControllerSucess() {
        guard let un = self.username.text else{
            return
        }
        let alertController = UIAlertController(title: nil, message: "Account \(un) sucessfully created", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: { action in
            self.dismiss(animated: true)
        })
        alertController.addAction(alertAction)
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func createAccount(_ sender: UIButton) {
        if self.hasAccssedFB == false{
            print("no")
            return
        }
        var ref: DatabaseReference!
        ref = Database.database().reference()
        guard let un = self.username.text else{
            return
        }
        var usersInfo = self.usersInfo ?? [String : Any]()
        var doesUserNameExist = false
        for (key,_) in usersInfo{
            let userKey = key
            guard let userInfo = usersInfo[userKey] as? NSDictionary else{
                continue
            }
            guard let userId = userInfo["username"] as? String else{
                continue
            }
            if decrypt(encryptedText: userId) == un{
                doesUserNameExist = true
            }
        }
        if doesUserNameExist == true{
            self.showAlertControllerDupe()
            return
        }
        let encryptedUsername = encrypt(plainText: un)
        print(encryptedUsername)
        let newUser = user.init(username: encryptedUsername, password: "NA", contacts: [])
        usersInfo[encryptedUsername] = newUser.to_dict()
        ref.setValue(usersInfo)
        self.usersInfo = usersInfo
        UserDefaults.standard.set(un, forKey: "username")
    }
    func getAllUsersFromFirebaseCompletionHandler(usersInfo : [String : Any]?){        
        self.usersInfo = usersInfo
        self.hasAccssedFB = true
    }
    func getAllUsersFromFirebase(){        
        let firebaseRealTimeDBRef = Database.database().reference()
        firebaseRealTimeDBRef.observeSingleEvent(of: .value, with: { (snapshot) in
            guard let usersInfo = snapshot.value as? [String : Any] else{
                self.getAllUsersFromFirebaseCompletionHandler(usersInfo: nil)
                return
            }
            self.getAllUsersFromFirebaseCompletionHandler(usersInfo: usersInfo)
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    @IBAction func backToLogin(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
