//
//  messagesVC.swift
//  Courier
//
//  Created by Sunil Ramakrishnan on 5/22/19.
//  Copyright © 2019 group5. All rights reserved.
//

import UIKit
import Firebase
import CryptoSwift

class messageCell : UITableViewCell{
    @IBOutlet weak var recieveMsg: UILabel!
    @IBOutlet weak var sendMsg: UILabel!
    func setCell(message : String, isUserMsg : Bool){
        if(isUserMsg == true){
            self.sendMsg.text = message
        }else{
            self.recieveMsg.text = message
        }
    }
}

class messagesVC: UIViewController,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var sendTextField: UITextField!
    @IBOutlet weak var messageTableView: UITableView!
    let messageCellStr = "Message"
    var contactToMessage : String?
    var currUser : String?
    var messages : [message] = []
    var userInstance : user?
    var contactInstance : user?
    var usersInfo : [String : Any]?
    var isContactToMsgAUser = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.messageTableView.delegate = self
        self.messageTableView.dataSource = self
        self.initializeScreen()
        self.observer()
    }
    @IBAction func backToContactsVC(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    func initializeScreen(){
        let firebaseRealTimeDBRef = Database.database().reference()
        firebaseRealTimeDBRef.observeSingleEvent(of: .value, with: { (snapshot) in
            guard let usersInfo = snapshot.value as? [String : Any] else{
                self.firebaseEndingCallback(usersInfo: nil)
                return
            }
            self.firebaseEndingCallback(usersInfo: usersInfo)
        }) { (error) in
            print(error.localizedDescription)
        }
        // authenticate with HMAC
        //
    }
    func observer(){
        let firebaseRealTimeDBRef = Database.database().reference()
        firebaseRealTimeDBRef.observe(.value, with: { (snapshot) in
            guard let usersInfo = snapshot.value as? [String : Any] else{
                self.firebaseEndingCallback(usersInfo: nil)
                return
            }
            self.firebaseEndingCallback(usersInfo: usersInfo)
        }) { (error) in
            print(error.localizedDescription)
        }
        // authenticate with HMAC
        //
    }
    func sortByMsgTimeStamp(lhs : message, rhs : message)->Bool{
        let lhsTimeStamp = lhs.timestamp
        let rhsTimeStamp = rhs.timestamp
        if lhsTimeStamp.year < rhsTimeStamp.year{
            return true
        }else if lhsTimeStamp.year > rhsTimeStamp.year{
            return false
        }
        
        if lhsTimeStamp.month < rhsTimeStamp.month{
            return true
        }else if lhsTimeStamp.month > rhsTimeStamp.month{
            return false
        }
        
        
        if lhsTimeStamp.day < rhsTimeStamp.day{
            return true
        }else if lhsTimeStamp.day > rhsTimeStamp.day{
            return false
        }
        
        if lhsTimeStamp.hour < rhsTimeStamp.hour{
            return true
        }else if lhsTimeStamp.hour > rhsTimeStamp.hour{
            return false
        }
        
        if lhsTimeStamp.minute < rhsTimeStamp.minute{
            return true
        }else if lhsTimeStamp.minute > rhsTimeStamp.minute{
            return false
        }
        
        return lhsTimeStamp.second < rhsTimeStamp.second
    }
    func firebaseEndingCallback(usersInfo : [String : Any]?){
        guard let usersInfo = usersInfo else{
            return
        }
        for (user,_) in usersInfo{
            let decryptedUser = decrypt(encryptedText: user)
            if decryptedUser == self.contactToMessage{
                self.isContactToMsgAUser = true
            }
        }
        self.usersInfo = usersInfo
        guard let currUser = self.currUser else{
            return
        }
        let encryptedCurrUser = encrypt(plainText: currUser)
        guard let contactToMsg = self.contactToMessage else{
            return
        }
        guard let userDict = usersInfo[encryptedCurrUser] as? [String : Any] else{
            return
        }
        guard let currUserInstance = user.dictToContact(userDict : userDict) else{
            return
        }
        self.userInstance = currUserInstance
        guard let uinst = self.userInstance else{
            return
        }
        let contacts = uinst.contacts
        let encryptedContactToMsg = encrypt(plainText: contactToMsg)
        for contact in contacts{
            if contact.contactId == encryptedContactToMsg{
                self.messages = contact.messages
            }
        }
        messages.sort(by: sortByMsgTimeStamp)
        self.messageTableView.reloadData()
    }
    @IBAction func sendMsg(_ sender: UIButton) {
        guard let msgToSend = self.sendTextField.text else{
            return
        }
        
        var ref: DatabaseReference!
        ref = Database.database().reference()
        guard let currUser = self.currUser else{
            return
        }
        guard let contactToMsg = self.contactToMessage else{
            return
        }
        let encryptedMsg = encrypt(plainText: msgToSend)
        let encryptedCurrUser = encrypt(plainText: currUser)
        let encryptedContactToMsg = encrypt(plainText: contactToMsg)
        
        let msgInstance = message.init(senderId: encryptedCurrUser, timestamp: timestamp1.fromDate(), messageContent: encryptedMsg)
        
        if self.isContactToMsgAUser == true{
            print("setting contact")
            guard var allUsers = self.usersInfo else{
                print("f3")
                return
            }
            guard let userToMessage = allUsers[encryptedContactToMsg] as? [String : Any] else{
                print("f2")
                return
            }
            guard let userToMessageInst = user.dictToContact(userDict: userToMessage) else{
                print("f1")
                return
            }
            let contacts = userToMessageInst.contacts
            for contact in contacts{
                if contact.contactId == encryptedCurrUser{
                    contact.messages.append(msgInstance)
                }
            }
            allUsers[encryptedContactToMsg] = userToMessageInst.to_dict()
            ref.setValue(allUsers)
            self.usersInfo = allUsers
        }
        
        guard var allUsers = self.usersInfo else{
            return
        }
        guard let userToMessage = allUsers[encryptedCurrUser] as? [String : Any] else{
            return
        }
        guard let userToMessageInst = user.dictToContact(userDict: userToMessage) else{
            return
        }
        let contacts = userToMessageInst.contacts
        for contact in contacts{
            if contact.contactId == encryptedContactToMsg{
                contact.messages.append(msgInstance)
            }
        }
        allUsers[encryptedCurrUser] = userToMessageInst.to_dict()
        ref.setValue(allUsers)
        self.initializeScreen()
        self.sendTextField.text = ""
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.messageTableView.dequeueReusableCell(withIdentifier: self.messageCellStr) as! messageCell
        let message = self.messages[indexPath.row]
        let decryptedSenderId = decrypt(encryptedText: message.senderId)
        let isUserMsg = (decryptedSenderId == self.currUser) ? true : false
        let decryptedMsg = decrypt(encryptedText: message.messageContent)
        cell.setCell(message: decryptedMsg, isUserMsg: isUserMsg)
        return cell
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
