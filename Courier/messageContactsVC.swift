//
//  messageContactsVC.swift
//  Courier
//
//  Created by Sunil Ramakrishnan on 5/21/19.
//  Copyright © 2019 group5. All rights reserved.
//

import UIKit
import Firebase

class userContacts : UITableViewCell{
    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    func setContactNameCell(contactName : String){
        self.contactName.text = contactName
        self.userImage = nil
    }
}

class messageContactsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var a : UITextField?
    var ac : UIAlertController?
    let userContactsCellId = "userCell"
    let toMessagesVC = "toMessages"
    var enteredEmailAddress : String?
    var enteredPassword : String?
    var haveLoadedAllData = false
    var usersInfo : [String : Any]!
    var contacts : [contact] = []
    @IBOutlet weak var contactTableView: UITableView!
    var contactToMessage : String?
    override func viewDidLoad() {
        print("logged in")
        super.viewDidLoad()
        self.contactTableView.delegate = self
        self.contactTableView.dataSource = self
        self.loadContactsFromFirebase()
        self.observer()
    }
    func loadContactsFromFirebase(){
        let firebaseRealTimeDBRef = Database.database().reference()
        firebaseRealTimeDBRef.observeSingleEvent(of: .value, with: { (snapshot) in
            guard let usersInfo = snapshot.value as? [String : Any] else{
                self.firebaseEndingCallback(usersInfo: nil)
                return
            }
            self.firebaseEndingCallback(usersInfo: usersInfo)
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    func observer(){
        let firebaseRealTimeDBRef = Database.database().reference()
        firebaseRealTimeDBRef.observe(.value, with: { (snapshot) in
            guard let usersInfo = snapshot.value as? [String : Any] else{
                self.firebaseEndingCallback(usersInfo: nil)
                return
            }
            self.firebaseEndingCallback(usersInfo: usersInfo)
        }) { (error) in
            print(error.localizedDescription)
        }
        // authenticate with HMAC
        //
    }
    func firebaseEndingCallback(usersInfo : [String : Any]?){
        guard let usersInfo = usersInfo else{
            return
        }
        self.usersInfo = usersInfo
        self.haveLoadedAllData = true
        print("callback")
        guard let uInfo = self.usersInfo else{
            return
        }
        guard let username = self.enteredEmailAddress else{
            return
        }
        let encryptedUsername = encrypt(plainText: username)
        guard let currentUser = uInfo[encryptedUsername] as? [String : Any] else{
            return
        }
        guard let contactsDictFB = currentUser["contacts"] as? [String : Any] else{
            return
        }
        var contactsDict : [[String : Any]] = []
        for (key,_) in contactsDictFB{
            guard let cd = contactsDictFB[key] as? [String : Any] else{
                continue
            }
            contactsDict.append(cd)
        }
        self.contacts = []
        for contactDict in contactsDict{
            guard let contact = contact.dictToContact(contactDict: contactDict) else{
                continue
            }
            self.contacts.append(contact)
        }
        print("reloading data")
        self.contactTableView.reloadData()
    }
    func configurationTextField(textField: UITextField!){
        if let tField = textField {     //Save reference to the UITextField
            self.a?.text = "Hello world"
        }
    }
    func showAlertControllerSucess(_ message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addTextField(configurationHandler: configurationTextField)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: { action in            
            guard let ac = self.ac else{
                return
            }
            guard let txtField = ac.textFields else{
                return
            }
            guard let textInfo = txtField[0].text else{
                return
            }
            self.addContactAndUpdateDB(newContact: textInfo)
        })
        alertController.addAction(alertAction)
        self.ac = alertController
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertControllerFail(_ message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    func addContactAndUpdateDB(newContact : String){
        
        print("hi")
        
        guard var uInfo = self.usersInfo else{
            print("hi1")
            return
        }
        guard let username = self.enteredEmailAddress else{
            print("hi2")
            return
        }
        let encryptedUsername = encrypt(plainText: username)
        guard var currentUser = uInfo[encryptedUsername] as? [String : Any] else{
            print("hi3")
            return
        }
        let encryptedNewContact = encrypt(plainText: newContact)
        let newContactInstance = contact.init(contactId: encryptedNewContact)
        if currentUser["contacts"] == nil{
            let contactIn : [String : Any] = [:]
            //contactIn[username] = newContactInstance.to_dict()
            currentUser["contacts"] = contactIn
        }
        guard var contactsDictFB = currentUser["contacts"] as? [String : Any] else{
            print("hi4")
            return
        }
        if contactsDictFB[encryptedNewContact] != nil{
            return
        }
        
        print("first part done")
        
        contactsDictFB[encryptedNewContact] = newContactInstance.to_dict()
        currentUser["contacts"] = contactsDictFB
        uInfo[encryptedUsername] = currentUser
        self.usersInfo = uInfo
        
        guard var uInfo2 = self.usersInfo else{
            print("hi5")
            return
        }
        let username2 = encryptedNewContact
        print(encryptedNewContact)
        guard var currentUser2 = uInfo2[username2] as? [String : Any] else{
            print("h6")
            return
        }
        if currentUser2["contacts"] == nil{
            let contactIn : [String : Any] = [:]
            //contactIn[username] = newContactInstance.to_dict()
            currentUser2["contacts"] = contactIn
        }
        guard var contactsDictFB2 = currentUser2["contacts"] as? [String : Any] else{
            print("hi7")
            return
        }
        if contactsDictFB2[encryptedUsername] != nil{
            print("hi8")
            return
        }
        let newContactInstance2 = contact.init(contactId: encryptedUsername)
        contactsDictFB2[encryptedUsername] = newContactInstance2.to_dict()
        currentUser2["contacts"] = contactsDictFB2
        uInfo[username2] = currentUser2
        self.usersInfo = uInfo
        
        var contactsDict : [[String : Any]] = []
        for (key,_) in contactsDictFB{
            guard let cd = contactsDictFB[key] as? [String : Any] else{
                print("hi9")
                continue
            }
            contactsDict.append(cd)
        }
        var ref: DatabaseReference!
        ref = Database.database().reference()
        ref.setValue(uInfo)
        self.contacts = []
        for contactDict in contactsDict{
            guard let contact = contact.dictToContact(contactDict: contactDict) else{
                continue
            }            
            self.contacts.append(contact)
        }
        print("reloading data")
        self.contactTableView.reloadData()
    }
    @IBAction func addContact(_ sender: UIButton) {
        self.showAlertControllerSucess("Enter Contact Name")
    }
    @IBAction func logOut(_ sender: UIButton) {
        if self.haveLoadedAllData == true{
            self.dismiss(animated: true)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let tableViewCell = tableView.cellForRow(at: indexPath) as? userContacts else{
            return
        }
        guard let contactName = tableViewCell.contactName.text else{
            return
        }
        self.contactToMessage = contactName
        if self.haveLoadedAllData == true{
            performSegue(withIdentifier: self.toMessagesVC, sender: self)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contacts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.contactTableView.dequeueReusableCell(withIdentifier: self.userContactsCellId) as! userContacts
        let idx = indexPath.row
        let contactName = self.contacts[idx].contactId
        let decryptedContactName = decrypt(encryptedText: contactName)
        cell.setContactNameCell(contactName: decryptedContactName)
        return cell
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.toMessagesVC{
            guard let destVC = segue.destination as? messagesVC else{
                return
            }
            destVC.contactToMessage = self.contactToMessage
            destVC.currUser = self.enteredEmailAddress
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
