//
//  userClass.swift
//  Courier
//
//  Created by Sunil Ramakrishnan on 5/23/19.
//  Copyright © 2019 sunil. All rights reserved.
//

import Foundation

class user{
    var username : String
    var password : String
    var contacts : [contact]
    init(username : String, password : String, contacts : [contact]) {
        self.username = username
        self.password = password
        self.contacts = contacts
    }
    init(username : String, password : String) {
        self.username = username
        self.password = password
        self.contacts = []
    }
    func to_dict()->[String : Any]{
        var userDict : [String : Any] = [:]
        userDict["username"] = self.username
        userDict["password"] = self.password
        var contactsDict : [String : Any] = [:]
        for (_,contact) in self.contacts.enumerated(){
            let contactName = contact.contactId
            contactsDict[contactName] = contact.to_dict()
        }
        userDict["contacts"] = contactsDict
        return userDict
    }
    static func dictToContact(userDict : [String : Any])->user?{
        guard let username = userDict["username"] as? String else{
            return nil
        }
        guard let password = userDict["password"] as? String else{
            return nil
        }
        guard let contactsDict = userDict["contacts"] as? [String : Any] else{            
            return nil
        }
        let userInstance = user.init(username: username, password: password)
        for (key,_) in contactsDict{
            guard let contactDict = contactsDict[key] as? [String : Any] else{
                continue
            }
            guard let contactInstance = contact.dictToContact(contactDict: contactDict) else{
                continue
            }
            userInstance.contacts.append(contactInstance)
        }
        return userInstance
    }
}

class contact{
    var contactId : String
    var messages : [message]
    let public_key : String
    init(contactId : String, messages : [message]) {
        self.contactId = contactId
        self.messages = messages
        
        self.public_key = create_public_key(id: contactId)
    }
    init(contactId : String) {
        self.contactId = contactId
        self.messages = []
        
        self.public_key = create_public_key(id: contactId)
    }
    func to_dict()->[String : Any]{
        var contactDict : [String : Any]  = [:]
        contactDict["contactId"] = self.contactId
        var messageDict: [String : Any] = [:]
        for (idx,message) in self.messages.enumerated(){            
            messageDict[String(idx)] = message.to_dict()
        }
        contactDict["messages"] = messageDict
        contactDict["public_key"] = self.public_key
        return contactDict
    }
    static func dictToContact(contactDict : [String : Any])->contact?{
        guard let contactId = contactDict["contactId"] as? String else{
            return nil
        }
        let contactInstance = contact.init(contactId: contactId)
        guard let messagesDict = contactDict["messages"] as? NSArray else{            
            return contactInstance
        }
        for messageDictA in messagesDict{
            guard let messageDict = messageDictA as? [String  :Any] else{
                continue
            }
            guard let msgInstance = message.dictToContact(messageDict: messageDict) else{
                continue
            }
            contactInstance.messages.append(msgInstance)
        }
        return contactInstance
    }
}

class message{
    var senderId : String
    var timestamp : timestamp1
    var messageContent : String
    init(senderId : String, timestamp : timestamp1, messageContent : String) {
        self.senderId = senderId
        self.timestamp = timestamp
        self.messageContent = messageContent
    }
    func to_dict()->[String : Any]{
        var msgDict : [String : Any] = [:]
        msgDict["senderId"] = self.senderId
        msgDict["timestamp"] = self.timestamp.to_dict()
        msgDict["messageContent"] = self.messageContent
        return msgDict
    }
    static func dictToContact(messageDict : [String : Any])->message?{
        guard let senderId = messageDict["senderId"] as? String else{
            return nil
        }
        guard let msgContent = messageDict["messageContent"] as? String else{
            return nil
        }
        guard let timeStamp = messageDict["timestamp"] as? [String : Any] else{
            return nil
        }
        guard let ts = timestamp1.dictToContact(timestampDict : timeStamp) else{
            return nil
        }
        return message.init(senderId: senderId, timestamp: ts, messageContent: msgContent)
    }
}

class timestamp1{
    var month,day,year,hour,minute,second : Int
    init(month : Int ,day : Int,year : Int,hour : Int,minute : Int, second : Int){
        self.month = month
        self.day = day
        self.year = year
        self.hour = hour
        self.minute = minute
        self.second = second
    }
    func to_dict()->[String : Any]{
        var timestampDict : [String : Any] = [:]
        timestampDict["month"] = self.month
        timestampDict["day"] = self.day
        timestampDict["year"] = self.year
        timestampDict["hour"] = self.hour
        timestampDict["minute"] = self.minute
        timestampDict["second"] = self.second
        
        return timestampDict
    }
    static func dictToContact(timestampDict : [String : Any])->timestamp1?{
        guard let month = timestampDict["month"] as? Int else{
            return nil
        }
        guard let day = timestampDict["day"] as? Int else{
            return nil
        }
        guard let year = timestampDict["year"] as? Int else{
            return nil
        }
        guard let hour = timestampDict["hour"] as? Int else{
            return nil
        }
        guard let minute = timestampDict["minute"] as? Int else{
            return nil
        }
        guard let second = timestampDict["second"] as? Int else{
            return nil
        }
        return timestamp1.init(month: month, day: day, year: year, hour: hour, minute: minute, second: second)
    }
    static func fromDate()->timestamp1{
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        
        return timestamp1.init(month: month, day: day, year: year, hour: hour, minute: minutes, second: second)
    }
    func display(){
        print(self.day)
        print(self.month)
        print(self.year)
    }
}
