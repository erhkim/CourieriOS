Sunil Ramakrishnan:

Done This Week:

I convinced our team to switch over to IOS. I figured out the way in which we plan on sending and recieving messages. Defined out the classes and member info needed to store users and their message info (in an unsecure way). I set up the UI skeleton for the project. I set up our app to read/write data to/from firebase.

TODO:
I plan on building out our message pipeline and setting up a listener thread to enable real-time message updates. I also need to connect the existing skeleton UI to our app and set constraints so that it can work with other devices? (not sure if this is a requirement). Once are able to have users pass and recieve messages in real-time i'll work with Pavit to secure their info i.e. use oauth flow to enable login, encrypt sent messages and decrypt recieved messages rather than storign plaintext

Challanges:
Not trying to do too much here. This app wont be going into production at the time we release it so its important to not be too granular with UI design and writing efficient code. Going beyond a bare-bones chat application?

Having to discard our work was annoying but its for the best. None of us have any experience with web development, so doing the most basic things will be time-consuming. At least here I can help the team if we run into trouble.

Commits:
https://github.com/PavitBath/CourieriOS/commit/3e17abdcea49271e26e23628c71c47899e19e6ec
https://github.com/PavitBath/CourieriOS/commit/d40e4a514165ffc652a6a1d8ef2142f91886edb5
https://github.com/PavitBath/CourieriOS/commit/ed5f394679d90b56e6d0ef6245d417e9e9249c8b
https://github.com/PavitBath/CourieriOS/commit/3e17abdcea49271e26e23628c71c47899e19e6ec

