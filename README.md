# CourieriOS

iOS platform for Courier.

- Pavit Bath
- Sunil Ramakrishnan
- Eric Kim

## App Overview

### Login Screen

validates if user exists in the database
touch id to validate user id- hashed user id stored in keychain if touch id is sucessful retrieves this hash and decrypts it if this is sucessful user is sucessfully logged in and segue to contact list is performed- need the same device you created account on for this to work

otherwise user can elect to create a new account

### Sign Up Screen

user can only create a unique username
only one username per device- if user tries to create another account on the same device old account is lost
entered user id hashed and stored in keychain- creating a link between device and username
if account sucessfull created user can login

### Message Contacts Screen

user can add new contacts (must be existing app users)
information is hashed prior to upload to database
otherwise user can select a contact to message
updates happen in real time

### Message Screen

user can send real time message to selected contact
updates occur in real time
sorted by timestamp

send message subroutine hashes the message before posting to database

### database

database is encryped and decrypted client side
only plaintext info is timestamp (this could be encrypted but makes it easier for implementation purposes with respect to sorting messages by timestamp)
need access to decryption algorithm to find out info about users and their messages i.e. all the info is useless without the decryption algotrithm

wrote a serialization / deserialization fxn to post and retrieve data 

## Cryptographic Structure

All of our cryptographic code is located and called from the `encryptDecrypt.swift` file.

### Sending & Receiving Messages and Metadata

Before sending a information, the plaintext is first encrypted in AES and then attached to an HMAC hash.

The function that calculates the resultant digest is `encrypt(plainText: String) -> String`. It is called in `LoginVC.swift` to encrypt and decrypt metadata, e.g. userid, contactid, in the function `validateIfUserExists()` for decryption, and in `signUpVC.swift` where it first encrypts the metadata under the function `createAccount()`.

The same applies to each message in `messagesVC.swift` under the functions `sendMsg()` and `tableView()`

```swift
var encryptedMsg: String = "_error_encrypting_".bytes.toHexString()
do {
    let encrypted = try AES(key: key, blockMode: CBC(iv: iv), padding: .pkcs7).encrypt(plainText.bytes)
    encryptedMsg = encrypted.toHexString()
} catch {
    print(error)
}
```

### Key Management

We use the built in keychain service to store our cryptographic keys and to create public keys.

The functions are also located in the file `encryptDecrypt.swift`.

In the function `create_public_key()` the public key is generated from the provided Security framework and stored in the local keychain instance.

This function is always called during the creation of a new contact in the contact class initializer function.

```swift
init(contactId : String, messages : [message]) {
    self.contactId = contactId
    self.messages = messages
    self.public_key = create_public_key(id: contactId)
}
init(contactId : String) {
    self.contactId = contactId
    self.messages = []
    self.public_key = create_public_key(id: contactId)
}
```
